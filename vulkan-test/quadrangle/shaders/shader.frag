#version 450

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

float from_srgb_to_linear_channel(float value) {
    if (value <= 0.04045)
        return value / 12.92;
    else
        return pow((value + 0.055) / (1.055), 2.4);
}

vec3 from_srgb_to_linear(vec3 srgb)
{
	return vec3(
		from_srgb_to_linear_channel(srgb.r),
		from_srgb_to_linear_channel(srgb.g),
		from_srgb_to_linear_channel(srgb.b)
	);
}

void main() {
    outColor = vec4(from_srgb_to_linear(texture(texSampler, fragTexCoord).rgb), 1.0f);
    //outColor = texture(texSampler, fragTexCoord);
}